import { configureStore } from "@reduxjs/toolkit";
import reducer from './stateManagement/MyReducer';

const store = configureStore({
    reducer: {
        user: userSlice.reducer
    }
});

export default store;