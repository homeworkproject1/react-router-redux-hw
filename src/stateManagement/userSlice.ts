import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { User } from "../models/user";

export const userSlice = createSlice({
  name: "user",
  initialState: {
    user: {} as User
  },
  reducers: {
    userSet: (state, action: PayloadAction<User>) => {
      console.log(action);
      state.user = action.payload;
    }
  }
});

// each case under reducers becomes an action
export const { userSet } = userSlice.actions;

export default userSlice.reducer;
