import React from 'react';
import logo from './logo.svg';
import './App.css';
import homepage from './Components/pages/homepage/homepage';
import login from './Components/pages/login/login'
import register from './Components/pages/register/register';
import { BrowserRouter, Route, Routes, Link } from 'react-router-dom';


function App() {
  return (
    
    <BrowserRouter>
      <Routes>
        <Route index element={<homepage />}></Route>
        <Route path="login" element={<login />}></Route>
        <Route path="register" element={<register />}></Route>
        <Route path="*" element={<span>404</span>}></Route>
      </Routes>
      <nav>
        <ul>
          <li>
            <Link to={'/login'}>Login page</Link>
          </li>
          <li>
            <Link to={'/register'}>Register page</Link>
          </li>
        </ul>
      </nav>
      
      <div className='App'><h1>Hello World!</h1></div>

    </BrowserRouter>
        
    
  )
}

export default App;
